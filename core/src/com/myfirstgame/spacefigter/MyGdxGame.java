package com.myfirstgame.spacefigter;
import com.myfirstgame.spacefigter.Sceny.ManagerScenyGry;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.myfirstgame.spacefigter.Sceny.Menu;

public class MyGdxGame extends ApplicationAdapter {
	private SpriteBatch batch;
	private ManagerScenyGry scenaGry;
    public static final int SZEROKOSC = 600;
    public static final int WYSOKOSC = 315;
	@Override
	public void create () {
		batch = new SpriteBatch();
		scenaGry = new ManagerScenyGry();
		scenaGry.push(new Menu(scenaGry));
		Gdx.gl.glClearColor(1, 1, 1, 1);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		scenaGry.zaktualizuj(Gdx.graphics.getDeltaTime());
		scenaGry.wygeneruj(batch);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
