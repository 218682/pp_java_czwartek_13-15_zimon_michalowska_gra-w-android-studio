package com.myfirstgame.spacefigter.obiekty;

import com.badlogic.gdx.graphics.Texture;
//import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.myfirstgame.spacefigter.MyGdxGame;

/**
 * Created by Rob Sonik on 18.04.2017.
 * Klasa przedstawia obiekt którym porusza się gracz
 */

public class Statek extends Sprite {
    private static final int GRAWITACJA = -15;

    private Vector3    pozycja;
    private Vector3    predkosc;
    private Rectangle  graniceStatku;
    private Texture    tekstura;
    //private Animation  animacjaStatku;
    boolean kolizja;

    /**
     * Konstruktor tworzący obiekt statku i jego parametry
     * a jego początkowe położenie na planszy określone jest przez podane parametry
     * @param x położenie na osi poziomej
     * @param y położenie na osi pionowej
     */
    public Statek(int x, int y){
        pozycja       = new Vector3(x,y,0);
        predkosc      = new Vector3(0,0,0);
        tekstura      = new Texture("statek.png"); //do zrobienia
        graniceStatku = new Rectangle(x, y , tekstura.getWidth(), tekstura.getHeight());
        kolizja       = false;
    }

    /**
     * Metoda aktualizująca położenie statku w przestrzeni
     * @param dt - czas miedzy obecną a poprzednią klatką, zależy od szybkości pracy urządzenia
     */
    public void zaktualizuj(float dt){
        predkosc.add(0,GRAWITACJA,0);
        predkosc.scl(dt);  //skalowanie predkosci by dostosowac ją do prędkości urządzenia
        if(!kolizja)
            pozycja.add(0, predkosc.y, 0);
        if(pozycja.y+tekstura.getHeight()> MyGdxGame.WYSOKOSC)
            pozycja.y=MyGdxGame.WYSOKOSC-tekstura.getHeight();
        if(pozycja.y<0)
            pozycja.y=0;
        predkosc.scl(1/dt);// powrót do wartości niemodyfikowanej by umożliwić poprawne skalowanie nowymi wartościami dt w przyszłości
        graniceStatku.setPosition(pozycja.x, pozycja.y);
    }
    /** powoduje wzbicie sie statku w górę */
    public void lot(){
        predkosc.add(0,GRAWITACJA*(-2),0);
    }
    /** zwraca pozycję na osi poziomej */
    public float getX(){
        return pozycja.x;
    }
    /** zwraca pozycję statku na osi pionowej */
    public float getY(){
        return pozycja.y;
    }
    /** zwraca szerokość statku */
    public float getWidth(){ return tekstura.getWidth(); }
    /** zwraca wysokość statku */
    public float getHeight(){return tekstura.getHeight(); }
    /** zwraca kontur statku do sprawdzania kolizji z innymi obiektami */
    public Rectangle getBounds(){
        return graniceStatku;
    }
    /** zwraca teksture która przedstawia statek gracza */
    public Texture getTexture() {return tekstura;}
}
