package com.myfirstgame.spacefigter.obiekty;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.myfirstgame.spacefigter.MyGdxGame;

/**
 * Created by Rob Sonik on 19.04.2017.
 */

public class MalyMeteoryt {
    private Vector3   pozycja;
    private Vector3   predkosc;
    private Rectangle granice;
    private Texture   tekstura;

    public MalyMeteoryt(int y, int tempo)
    {
        pozycja  = new Vector3(MyGdxGame.SZEROKOSC,y,0);
        predkosc = new Vector3(-tempo,0,0);
        tekstura = new Texture("malymeteor.png"); //do zrobienia
        granice  = new Rectangle(MyGdxGame.SZEROKOSC, y , tekstura.getWidth(), tekstura.getHeight());
    }
    public void zaktualizuj(float dt){
        predkosc.scl(dt);  //skalowanie predkosci by dostosowac ją do prędkości urządzenia
        pozycja.add(predkosc.x, 0, 0);
        predkosc.scl(1/dt);// powrót do wartości niemodyfikowanej by umożliwić poprawne skalowanie nowymi wartościami dt w przyszłości
        granice.setPosition(pozycja.x, pozycja.y);
    }
    /** zwraca pozycję na osi poziomej */
    public float getX(){
        return pozycja.x;
    }
    /** zwraca pozycję statku na osi pionowej */
    public float getY(){
        return pozycja.y;
    }
    /** zwraca szerokość */
    public float getWidth(){ return tekstura.getWidth(); }
    /** zwraca wysokość*/
    public float getHeight(){return tekstura.getHeight(); }
    /** zwraca kontur do sprawdzania kolizji z innymi obiektami */
    public Rectangle getBounds(){
        return granice;
    }
    /** zwraca teksture która przedstawia meteoryt */
    public Texture getTexture() {return tekstura;}
    /**
     *
     * @param statek - obwód statku zapisany jako obiekt Rectangle
     * @return czy obszary sie pokrywaja
     */
    public boolean zderzenie(Rectangle statek){return statek.overlaps(granice);}
}
