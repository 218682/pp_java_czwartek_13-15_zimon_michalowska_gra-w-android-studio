package com.myfirstgame.spacefigter.Sceny;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Klasa po której dziedziczą wszystkie sceny np. menu
 */
public abstract class Scena {
    protected ManagerScenyGry msg;
    protected OrthographicCamera camera;
    Scena(ManagerScenyGry msg){
        this.msg=msg;
        camera= new OrthographicCamera();
        camera.setToOrtho(true, 400.0F, 240.0F);
    }
    public abstract void obsluzSterowanie();       //metoda oblugujaca sterowanie w danej scenie
    public abstract void zaktualizuj(float dt);    //metoda aktualizujaca wydarzenia na danej scenie/ robiąca obliczenia
    public abstract void wygeneruj(SpriteBatch sb);//metoda generujaca obraz danej sceny
}
