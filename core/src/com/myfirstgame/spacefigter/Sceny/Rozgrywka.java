package com.myfirstgame.spacefigter.Sceny;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.myfirstgame.spacefigter.MyGdxGame;
import com.myfirstgame.spacefigter.obiekty.MalyMeteoryt;
import com.myfirstgame.spacefigter.obiekty.Statek;

import java.util.Random;


/**
 * Najważniejsza ze scen, przedstawia główną rozgrywkę oraz obsługuje zdarzenia,
 * które sa z nią związane takie jak wyświetlanie zmian i sprawdzanie czy gracz
 * nie zderzył się z jakimś obiektem
 */

public class Rozgrywka extends Scena{
    private Texture tlo;
    private Statek  statek;
    private boolean gameover;
    private Array<MalyMeteoryt> maleMeteoryty;
    private Random losowa;
    private int punkty;
    public Rozgrywka(ManagerScenyGry managerScenyGry){
        super(managerScenyGry);
        tlo           = new Texture("tlo.png");     //wczytujemy tekstury
        statek        = new Statek(40, MyGdxGame.WYSOKOSC/2);         // ustawiamy statek w pozycji poczatkowej
        losowa        = new Random();
        maleMeteoryty = new Array<MalyMeteoryt>();
        gameover = false;
        punkty   = 0;
        camera.setToOrtho(false, MyGdxGame.SZEROKOSC, MyGdxGame.WYSOKOSC);
    }

    @Override
    public void obsluzSterowanie() {
        if(!gameover) {
            if (Gdx.input.isTouched())
                statek.lot();
        }
        else
            if (Gdx.input.justTouched()) {
                msg.set(new Menu(msg));
            }
    }

    @Override
    public void zaktualizuj(float dt) {
        float pomocnicza;
        int pomocnicza2;
        int pomocnicza3;
        obsluzSterowanie();
        if(!gameover){
            statek.zaktualizuj(dt);
            if(statek.getY()<=0) {
                gameover = true;
            }
            if(statek.getY()>=MyGdxGame.WYSOKOSC-statek.getTexture().getHeight()) {
                gameover = true;
            }
            pomocnicza=losowa.nextFloat();
            if(pomocnicza+dt>1) {
                pomocnicza3=losowa.nextInt(50);
                pomocnicza2=losowa.nextInt(MyGdxGame.WYSOKOSC);
                maleMeteoryty.add(new MalyMeteoryt(pomocnicza2, pomocnicza3+25));
            }
            for (MalyMeteoryt malyMeteoryt : maleMeteoryty) {
                malyMeteoryt.zaktualizuj(dt);
                if(malyMeteoryt.zderzenie(statek.getBounds()))
                    gameover=true;
                if (malyMeteoryt.getX() < 0) {
                    punkty++;
                    malyMeteoryt = null;
                }
                maleMeteoryty.removeValue(null, true);
            }
            //camera.update();
        }
    }

    @Override
    public void wygeneruj(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);

        sb.begin();
        sb.draw(statek.getTexture(), statek.getX(), statek.getY());
        for (MalyMeteoryt malyMeteoryt : maleMeteoryty) {
            sb.draw(malyMeteoryt.getTexture(), malyMeteoryt.getX(), malyMeteoryt.getY());
        }
        sb.end();
    }
}
