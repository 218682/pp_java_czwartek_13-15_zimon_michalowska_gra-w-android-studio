package com.myfirstgame.spacefigter.Sceny;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.myfirstgame.spacefigter.MyGdxGame;

/**
 * Scena z najlepszymi wynikami graczy
 */

public class Wynik extends Scena{
    private Texture tlo;

    public Wynik(ManagerScenyGry managerScenyGry) {
        super(managerScenyGry);
        tlo                 = new Texture("tlo.png");     //wczytujemy tekstury
    }

    @Override
    public void obsluzSterowanie() {
        // tu klikniecie jakiegoś przycisku powrotu do menu?
        if(Gdx.input.isTouched())
            msg.pop();
    }

    @Override
    public void zaktualizuj(float dt) {
        //wczytywanie danych z serwera dotyczących najwyższych wyników
        obsluzSterowanie(); //i sprawdzenie sterowania
    }

    @Override
    public void wygeneruj(SpriteBatch sb) {
        // rysowanie wyników
        sb.begin();
        sb.draw(tlo, 0, 0, MyGdxGame.SZEROKOSC, MyGdxGame.WYSOKOSC);
        sb.end();
    }
}
