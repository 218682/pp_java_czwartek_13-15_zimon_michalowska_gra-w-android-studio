package com.myfirstgame.spacefigter.Sceny;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.myfirstgame.spacefigter.MyGdxGame;

/**
 * Created by Rob Sonik on 17.04.2017.
 */

public class Instrukcja extends Scena{
    private Texture tlo;
    public Instrukcja(ManagerScenyGry managerScenyGry){
        super(managerScenyGry);
        tlo                 = new Texture("tlo.png");     //wczytujemy tekstury, najlepiej od razu z instrukcja
    }
    @Override
    public void obsluzSterowanie() {
        //
        if(Gdx.input.isTouched()) {
            msg.pop();
        }
    }

    @Override
    public void zaktualizuj(float dt) {
        //tu wlasciwie niewiele do zrobienia
        obsluzSterowanie(); //poza sprawdzeniem sterowania
    }

    @Override
    public void wygeneruj(SpriteBatch sb) {
        // wyrysowanie instrukcji
        sb.begin();
        sb.draw(tlo, 0, 0, MyGdxGame.SZEROKOSC, MyGdxGame.WYSOKOSC);
        sb.end();
    }
}
