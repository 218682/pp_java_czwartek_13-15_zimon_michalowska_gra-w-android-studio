package com.myfirstgame.spacefigter.Sceny;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Stack;

/**
 * klasa zarządzająca i przechowująca sceny naszej gry np. scena Menu, scena Wyniki itp.
  */
public class ManagerScenyGry {
    private Stack<Scena> sceny;

    public ManagerScenyGry(){
        sceny = new Stack<Scena>();
    }

    public void push(Scena scena){
        sceny.push(scena);
    }

    public void pop(){
        sceny.pop();
    }

    public void set(Scena scena){
        sceny.pop();
        sceny.push(scena);
    }

    public void zaktualizuj(float dt){
        sceny.peek().zaktualizuj(dt);
    }

    public void wygeneruj(SpriteBatch sb){
        sceny.peek().wygeneruj(sb);
    }
}
