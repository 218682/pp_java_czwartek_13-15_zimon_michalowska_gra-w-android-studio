package com.myfirstgame.spacefigter.Sceny;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.myfirstgame.spacefigter.MyGdxGame;

/**
 * Początkowa scena gry, są w niej wyświetlane przyciski, które prowadzą gracza do innych scen
 */

public class Menu extends Scena{
    private Texture tlo;
    private Texture przyciskPlay;       //do zamienienia przez TextButton
    private Texture przyciskHelp;
    private Texture przyciskHighScores;
    //private TextButton przyciskGra;
    //private Skin przyciski;             // potrzebny plik JSON
    public Menu(ManagerScenyGry managerScenyGry) {
        super(managerScenyGry);
        tlo                 = new Texture("tlo.png");     //wczytujemy tekstury
        przyciskPlay        = new Texture("playbtn.png");
        przyciskHelp        = new Texture("helpbtn.png");
        przyciskHighScores  = new Texture("hsbtn.png");
        //przyciski           = new Skin();

        //przyciskGra         = new TextButton("Play", )
    }

    @Override
    public void obsluzSterowanie() {
        int pozycjaX, pozycjaY;
        if(Gdx.input.justTouched()){
            pozycjaX=Gdx.input.getX();   //zapisujemy pozycje, którą klknął gracz
            pozycjaY=Gdx.input.getY();
            if(pozycjaX > MyGdxGame.SZEROKOSC / 2 - przyciskPlay.getWidth()          // sprawdzamy klikniętą pozycje
                    && (pozycjaX < ((MyGdxGame.SZEROKOSC / 2) + przyciskPlay.getWidth()))){       // by kreślić który guzik został naciśnięty
                if(pozycjaY>MyGdxGame.WYSOKOSC-(przyciskPlay.getHeight()/2))
                    msg.set(new Rozgrywka(msg));
                else if(pozycjaY>(MyGdxGame.WYSOKOSC/2-przyciskHelp.getHeight()/2)
                && pozycjaY<(MyGdxGame.WYSOKOSC/2+przyciskHelp.getHeight()/2))
                    msg.push(new Instrukcja(msg));
                else if(pozycjaY<przyciskHighScores.getHeight())
                    msg.push(new Wynik(msg));
            }

        }
    }

    @Override
    public void zaktualizuj(float dt) {
        obsluzSterowanie();
    }

    @Override
    public void wygeneruj(SpriteBatch sb) {
        sb.begin();
        sb.draw(tlo, 0, 0, MyGdxGame.SZEROKOSC, MyGdxGame.WYSOKOSC);
        // rysuj przyciski w wyznaczonych miejscach
        sb.draw(przyciskPlay, (MyGdxGame.SZEROKOSC / 2) - (przyciskPlay.getWidth() / 2),
                MyGdxGame.WYSOKOSC-(przyciskPlay.getHeight()));

        sb.draw(przyciskHelp, (MyGdxGame.SZEROKOSC / 2) - (przyciskPlay.getWidth() / 2),
                MyGdxGame.WYSOKOSC / 2);

        sb.draw(przyciskHighScores, (MyGdxGame.SZEROKOSC / 2) - (przyciskPlay.getWidth() / 2),
                przyciskHighScores.getHeight()/2);
        sb.end();

    }
}
